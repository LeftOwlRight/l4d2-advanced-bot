::NavigatorPause <- {}

class ::Navigator {
    pathCache = {};
    player = null;
    movingID = null;
    lastArea = null;
    buildCoolDown = {};
    faildTimes = {};
    timeOut = Time();

	constructor(playerIn) {
        player = playerIn;
        movingID = null;
        pathCache = {};
        lastArea = playerIn.GetLastKnownArea();
        buildCoolDown = {};
        faildTimes = {};
        timeOut = Time();
    }
	
    function _typeof () {
        return "Navigator";
    }

	function buildPath(goal, id, priority = 0, discardFunc = BotAI.trueDude, previousPath = null, aStar = false, distance = 4000) {
        if(timeOut > Time())
            return false;
        local goalArea = null;
        local goalPos = null;
        if(typeof goal == "Vector") {
            goalPos = goal;
        } else if(BotAI.IsEntityValid(goal)){
            if("GetLastKnownArea" in goal)
                goalArea = goal.GetLastKnownArea();
            goalPos = goal.GetOrigin();
        } else 
            return false;
        
        if(goalArea == null) {
            goalArea = NavMesh.GetNavArea(goalPos, 150);
            if(goalArea == null && BotAI.IsTriggerUsable(goal)) {
                local direcVec = Vector(120, 0, 0);
                for(local i = 0; i < 8; ++i) {
					local angleVec = BotAI.rotateVector(direcVec, i * 45);
                    local targetPos = goalPos + angleVec;
					if(BotAI.BOT_AI_TEST_MOD == 1) {
						DebugDrawCircle(targetPos, Vector(0, 0, 255), 1.0, 5, true, 1.0);
						DebugDrawText(targetPos, "goal", false, 1.0);
					}

                    goalArea = NavMesh.GetNavArea(targetPos, 150);
                    if(goalArea != null) break;
				}
            }
        }
            
        
        if(typeof previousPath == "table") {
            local newPaths = [];
            for(local i = 0; i < previousPath.len(); ++i) {
                if(("area" + i.tostring()) in previousPath)
                	newPaths.append(previousPath["area" + i.tostring()]);
            }
            previousPath = newPaths;
        }

        local paths = {};
        local build = false;
        local buildAStar = false;
        local goalInCoolDown = goal in buildCoolDown;
        if(!goalInCoolDown)
        foreach(object, cooldown in buildCoolDown) {
            if(BotAI.isEntityEqual(object, goal))
                goalInCoolDown = true;
        }
        if(!goalInCoolDown) {
            if(aStar) {
                try {
                    build = createPath(goalPos, goalArea, distance, paths, previousPath);
                }
                catch(e) {
                    timeOut = Time() + 1;
                    printl("[Navigator] Path build time out.");
                }
            }
            else {
                try {
                    build = createPath(goalPos, goalArea, distance, paths);
                }
                catch(e) {
                    timeOut = Time() + 1;
                    printl("[Navigator] Path build time out.");
                }
            }
        }

        if(!build) {
            if(!goalInCoolDown) {
                local count = 200;
                if(goal in faildTimes) {
                    faildTimes[goal] <- faildTimes[goal] + 1;
                    count *= faildTimes[goal];
                }
                else
                    faildTimes[goal] <- 1;
                buildCoolDown[goal] <- count;
            }
            
            if( BotAI.BOT_AI_TEST_MOD == 1) {
                printl(id + " A-Star build faild." + " goal " + goal);
            }
            local faildBuild = false;
            //if(navAPI)
                //faildBuild = !createPath(goalPos, goalArea, paths);
            
            /*
            if(!faildBuild && navAPI) {
                paths = {};
                build = NavMesh.GetNavAreasFromBuildPath(player.GetLastKnownArea(), null, goalPos, 9999, 2, false, paths);
                printl("build: " + build);
            }
            */
        } else {
            if(BotAI.BOT_AI_TEST_MOD == 1)
                printl("A-Star build success.");
            
            buildAStar = true;
            if(goal in faildTimes)
                delete faildTimes[goal];
        }
            
            
        /*
        if(build && paths.len() > 0) {
            local firstArea = paths["area" + (paths.len() - 1)];
            if(firstArea.IsBlocked(2, true) || !firstArea.IsValid()) {
                return false;
            }
        }
        */
        if(build) {
            if(BotAI.BOT_AI_TEST_MOD == 1)
                printl(id + " build complete.");
            local data = PathData(paths, goal, priority, discardFunc, distance);

            if(buildAStar)
                data.aStar = true;
            pathCache[id] <- data;
        } else if(id.find("ping") != null) {
            player.SetOrigin(goalPos);
        }

        return build;
    }

    function shouldDiscard() {
        if(!moving()) return;
        if(getRunningPathData().discardFunc()) {
            if(BotAI.BOT_AI_TEST_MOD == 1)
                printl("[Navigator] Discard: " + movingID);
            stop();
        }
    }

    function onUpdate() {
        if(BotAI.BOT_AI_TEST_MOD == 1) {
            local height = 20;
            if(moving()) {
                DebugDrawText(player.EyePosition() + Vector(0, 0, height), "running " + movingID + " priority: " + pathCache[movingID].priority, false, 0.2);
                height += 10;
            }
            foreach(name, pathGet in pathCache) {
                if(moving() && name == movingID) continue;
                DebugDrawText(player.EyePosition() + Vector(0, 0, height), name.tostring() + " priority: " + pathGet.priority + " wating", false, 0.2);
                height += 10;
            }
        }
        foreach(idx, val in buildCoolDown) {
            buildCoolDown[idx] <- val - 1;
            if(val < 2)
                delete buildCoolDown[idx];
        }

        foreach(idx, func in NavigatorPause) {
            if(func(player)) {
                return;
            } 
        }
        if(pathCache.len() < 1) {
            return;
        }
        
        reRun();
        
        shouldDiscard();
        if(!moving()) {
            return;
        }

        if(BotAI.BOT_AI_TEST_MOD != 2 && !BotAI.HasTank) {
            local friction = 0.8;
            player.OverrideFriction(0.5, friction);
        }
        
		recordLastArea();
		local paths = getRunningPathData().paths;
        local offset = 25;
		if(paths != null && BotAI.validVector(getRunningPathData().getPos())) {
			if(BotAI.IsOnGround(player)) {
                BotAI.DisableButton(player, BUTTON_WALK, 1.0);
            }
			local goalPos = getRunningPathData().getPos();
			local xyGoalPos = goalPos;
			local xyTracePos = BotAI.tracePos(player, goalPos, true);
            local addition = Vector(0, 0, 5);
			if(BotAI.BOT_AI_TEST_MOD == 1) {
			    DebugDrawCircle(xyGoalPos, Vector(0, 0, 255), 1.0, 5, true, 0.2);
			    DebugDrawLine(player.EyePosition(), xyGoalPos, 0, 0, 255, true, 0.2);
            }
            
            if(BotAI.BOT_AI_TEST_MOD == 1 && BotAI.validVector(xyTracePos)) {
				DebugDrawCircle(xyTracePos + addition, Vector(0, 255, 0), 1.0, 10, true, 0.2);
				DebugDrawLine(player.EyePosition() + addition, xyTracePos + addition, 0, 255, 0, true, 0.2);
			}

            local canTrace = false;
            if(BotAI.distanceof(xyGoalPos, xyTracePos) <= offset) {
                if(xyGoalPos.z <= xyTracePos.z)
                    canTrace = true;
            }
			if(canTrace) {
                if(BotAI.distanceof(player.GetOrigin(), goalPos) > 10)
				    BotAI.botRun(player, goalPos, 400);
                else if(movingID.find("#") != null) {
                    player.OverrideFriction(0.5, 10);
                    player.SetVelocity(Vector(0, 0, player.GetVelocity().z));
                }
                    
				if(movingID.find("#") == null && BotAI.distanceof(BotAI.fakeTwoD(player.GetOrigin()), BotAI.fakeTwoD(goalPos)) <= offset) {
					stop();
				}
				return;
			}

			if(paths.len() <= 0){
                if((goalPos.z - player.GetOrigin().z) >= 56)
				    player.SetOrigin(goalPos);
                return;
            }

            try {
                local throwError = paths["area0"];
            }
            catch(e) {
                printl("[the index 'area' does not exist]");
                printl("[index size]: " + paths.len());
                BotAI.printTable(paths)
            }

			if(BotAI.BOT_AI_TEST_MOD == 1) {
			    for(local i = 0; i < paths.len(); ++i) {
                    if(("area" + i) in paths) {
                        local path = paths["area" + i];
            	        path.DebugDrawFilled(0, 255, 0, 15, 0.2, true);
            	        DebugDrawText(path.GetCenter(), i.tostring(), false, 0.2);
                    } 
        	    }
            }

			local firstArea = ("area" + (paths.len() - 1)) in paths ? paths["area" + (paths.len() - 1)] : paths["area" + paths.len()];
			
			if(firstArea.IsDamaging()) {
                return;
            }

			if(firstArea != player.GetLastKnownArea()) {
                local lastArea = player.GetLastKnownArea();
                local direc = firstArea.ComputeDirection(lastArea.GetCenter());
                local lowCorner = BotAI.getLowOriginFromArea(firstArea, direc);
                local highCorner = BotAI.getHighOriginFromArea(lastArea, lastArea.ComputeDirection(firstArea.GetCenter()));
                local bottleneckLowCorner = BotAI.getLowOriginFromArea(lastArea, lastArea.ComputeDirection(firstArea.GetCenter()));
                local bottleneckHighCorner = BotAI.getHighOriginFromArea(firstArea, direc);
                local height = lowCorner.z - highCorner.z;
                if(height >= 56) {
                    if(BotAI.BOT_AI_TEST_MOD == 1)
                        printl("[Nagigator] lowCorner height: " + height);
                    player.SetOrigin(firstArea.GetCenter());
                } else if(height > 10 && BotAI.distanceof(lowCorner, player.GetOrigin()) < 20 && BotAI.IsOnGround(player)) {
                    BotAI.ForceButton(player, 2 , 0.2);
                }

                if(lastArea.IsBottleneck() || (bottleneckHighCorner.z - bottleneckLowCorner.z) >= 100) {
                    BotAI.ForceButton(player, 2 , 0.2);
                }

                local origins = BotAI.getDirectionOriginFromArea(firstArea, direc);
	            local origin0 = origins[0];
	            local origin1 = origins[1];
                local origin = Vector((origin0.x + origin1.x) * 0.5, (origin0.y + origin1.y) * 0.5, (origin0.z + origin1.z) * 0.5);
                local xyCenter = Vector((firstArea.GetCenter().x + origin.x) * 0.5, (firstArea.GetCenter().y + origin.y) * 0.5, (firstArea.GetCenter().z + origin.z) * 0.5);
				local xyAreaPos = BotAI.tracePos(player, xyCenter);
                if(BotAI.BOT_AI_TEST_MOD == 1 && BotAI.tickExisted % 10 == 0) {
                    printl("origin0 " + origin0);
                    printl("origin1 " + origin1);
                    printl("origin " + origin);
                    printl("xyCenter " + xyCenter);
                }
                if(BotAI.BOT_AI_TEST_MOD == 1) {
				    DebugDrawCircle(xyCenter, Vector(255, 255, 0), 1.0, 5, true, 0.2);
				    DebugDrawLine(player.EyePosition(), xyCenter, 255, 255, 0, true, 0.2);
				    if(BotAI.validVector(xyAreaPos)) {
					    DebugDrawCircle(xyAreaPos + addition, Vector(255, 0, 255), 1.0, 5, true, 0.2);
					    DebugDrawLine(player.EyePosition() + addition, xyAreaPos + addition, 255, 0, 255, true, 0.2);
				    }
                }

				local traceFirst = BotAI.distanceof(xyCenter, xyAreaPos) <= offset;
                
				if(traceFirst)
					BotAI.botRun(player, xyCenter, 400);
				else
					BotAI.botRun(player, lastArea.GetCenter(), 400);[]
			}
		}
    }

    function clearPath(id) {
        if(id == movingID)
            stop();
        if(id in pathCache)
            delete pathCache[id];
    }

    function moving() {
        return movingID != null && movingID != "";
    }

    function isMoving(id) {
        if(id == movingID)
            return true;
        return false;
    }

    function run(id) {
        if(!(id in pathCache)) { return; }
        if(moving()) {
            local runPath = pathCache[id];
            local runningPath = getRunningPathData();

            if(runPath.priority >= runningPath.priority)
                movingID = id;
        } else
             movingID = id;
    }

    function reRun() {
        if(moving() || pathCache.len() < 1) return;
        local pathID = "";
        local pathFound = null;
        foreach(id, path in pathCache) {
            if(pathFound == null || path.priority > pathFound.priority) {
                if(!path.discardFunc()) {
                    pathID = id;
                    pathFound = path;
                } else 
                    clearPath(id);
            }
        }

        if(pathID != "")
            run(pathID);
    }

    function hasPath(id) {
        return getMovingPath(id) != null;
    }

    function stop() {
        local id = movingID;
        movingID = null;
        if(id in pathCache)
            delete pathCache[id];
    }

    function getMovingPath(id) {
        if(!(id in pathCache)) return null;
        return pathCache[id];
    }

    function getRunningPathData() {
        if(moving() && movingID in pathCache) 
            return pathCache[movingID];
        return null;
    }

    function recordLastArea() {
        if(lastArea != player.GetLastKnownArea()) {
            lastArea = player.GetLastKnownArea();
            if(moving()) {
                local data = getRunningPathData();
                local discard = data.discard;

                if("func" in discard)
                    buildPath(data.pos, movingID, data.priority, discard["func"], data.paths, data.aStar, data.distance);
                else
                    buildPath(data.pos, movingID, data.priority, BotAI.trueDude, data.paths, data.aStar, data.distance);
            }
        }
    }

    function createPath(pos, endArea, distance, paths, previousPath = null, startPos = null, startArea = null) {
        if(endArea == null)
            endArea = NavMesh.GetNavArea(pos + Vector(0, 0, 50), 200);
        if(endArea == null)
            endArea = NavMesh.GetNearestNavArea(pos + Vector(0, 0, 50), 200, true, true);

        if(endArea == null)
            return false;
        //endArea.DebugDrawFilled(0, 0, 255, 100, 10, true);
        local re = false;
        local checked = {};
        local needCheack = {};
        local deleteFirstArea = false;
        if(startPos == null && startArea == null) {
            startPos = player.GetOrigin();
            startArea = player.GetLastKnownArea();
            deleteFirstArea = true;
        } else
            re = true;

		local playerArea = AreaData(startArea, null, 0, BotAI.distanceof(startPos, pos));
        if(startArea != null)
            needCheack[startArea.GetID()] <- playerArea;

		while(needCheack.len() > 0) {
            local mostSuitable = null;
            foreach(idx, areaData in needCheack) {
			    if(mostSuitable == null || (areaData.exactCost + areaData.estimatedCost) < (mostSuitable.exactCost + mostSuitable.estimatedCost))
                    mostSuitable = areaData;
            }

            if(typeof previousPath == "array" && previousPath.find(mostSuitable.area)) {
                local idx = previousPath.find(mostSuitable.area);
                local cachePaths = [];
                local newPaths = [];
                local function filter(index, val) {
                    return index < idx;
                }
                previousPath = previousPath.filter(filter);
                local areaData = mostSuitable;
                while(areaData != null) {
                    cachePaths.append(areaData.area);
                    areaData = areaData.lastArea;
                }

                local len = cachePaths.len() + previousPath.len();

                newPaths.extend(previousPath);
                newPaths.extend(cachePaths);

                if(re) {
                    newPaths.reverse();
                    foreach(idx, path in newPaths) {
                        if(path != player.GetLastKnownArea())
                            paths["area" + idx] <- path;
                    }
                    return true;
                }
                else {
                    newPaths.reverse();
                    local build = createPath(player.GetOrigin(), player.GetLastKnownArea(), distance, paths, newPaths, pos, endArea);
                    if(build)
                        return true;
                }
            }
            
			for(local i = 0; i < 4; ++i) {
                local adjacentAreas = BotAI.areaAdjacent(mostSuitable.area, i);
                //if( i < 2 ) {
                local ladders = {};
                mostSuitable.area.GetLadders(i, ladders);
                foreach(ladder in ladders) {
                    if(ladder.IsValid() && ladder.IsUsableByTeam(2)) {
                        if("GetID" in ladder.GetBottomArea() && ladder.GetBottomArea().GetID() == mostSuitable.area.GetID())
                            adjacentAreas["area" + adjacentAreas.len()] <- ladder.GetTopArea();
                        else if("GetID" in ladder.GetTopArea() && ladder.GetTopArea().GetID() == mostSuitable.area.GetID())
                            adjacentAreas["area" + adjacentAreas.len()] <- ladder.GetBottomArea();
                    }
                }

                //}
				foreach(adjacent in adjacentAreas) {
				    if(!adjacent.IsValid() || adjacent.IsBlocked(2, false) || adjacent.IsDamaging()) continue;
                    local dir = 0;
                    if(i == 0)
                        dir = 2;
                    if(i == 1)
                        dir = 3;
                    if(i == 3)
                        dir = 1;
                    local lowCorner = BotAI.getLowOriginFromArea(adjacent, dir);
                    local highCorner = BotAI.getHighOriginFromArea(mostSuitable.area, i);
                    if(lowCorner.z - highCorner.z > 60) {
                        continue;
                    }

                    local exactCost = mostSuitable.exactCost + BotAI.distanceof(adjacent.GetCenter(), mostSuitable.area.GetCenter());
                    if(exactCost > distance)
                        continue;

                    if(adjacent.GetID() in checked) {
                        if(mostSuitable.lastArea == null)
                            continue;
                        local checkedArea = checked[adjacent.GetID()];
                        if(checkedArea.exactCost < mostSuitable.lastArea.exactCost) {
                            local highLayer = BotAI.getHighOriginFromArea(checkedArea.area, dir);
                            local lowLayer = BotAI.getLowOriginFromArea(mostSuitable.area, i);
                            if(lowLayer.z - highLayer.z <= 60) {
                                local updatePath = AreaData(mostSuitable.area, checkedArea, checkedArea.exactCost + BotAI.distanceof(mostSuitable.area.GetCenter(), checkedArea.area.GetCenter()), mostSuitable.estimatedCost);
                                mostSuitable = updatePath;
                                checked[mostSuitable.area.GetID()] <- updatePath;
                            }
                        }
                        continue;
                    }
                    if((endArea != null && adjacent.GetID() == endArea.GetID()) || adjacent.ContainsOrigin(pos)) {
                        local pathBack = adjacent;
                        local areaData = mostSuitable;
                        while(areaData != null && areaData.area != player.GetLastKnownArea()) {
                            paths["area" + paths.len()] <- pathBack;
                            pathBack = areaData.area;
                            areaData = areaData.lastArea;
                        }
                        if(BotAI.BOT_AI_TEST_MOD == 1) {
                            printl("[Navigator] search area: " + checked.len());
                            printl("[Navigator] search distance: " + mostSuitable.exactCost + mostSuitable.estimatedCost);
                        }
                        return true;
                    }
                    local area = AreaData(adjacent, mostSuitable, exactCost, BotAI.distanceof(adjacent.GetCenter(), pos));
                    //local area = AreaData(adjacent, BotAI.distanceof(adjacent.GetCenter(), pos), mostSuitable);
					needCheack[adjacent.GetID()] <- area;
                    checked[adjacent.GetID()] <- area;
                    if(checked.len() > 1200) {
                        if(BotAI.BOT_AI_TEST_MOD == 1) {
                            printl("[Navigator] search over 1200 area!");
                            printl("[Navigator] search distance: " + mostSuitable.exactCost + mostSuitable.estimatedCost);
                        }
                        return false;
                    }
				}
			}

            if(mostSuitable != null) {
                delete needCheack[mostSuitable.area.GetID()];
                //mostSuitable.area.DebugDrawFilled(255, 0, 0, 30, 10, true);
            }
		}
        return false;
    }
}

class ::PathData {
    paths = {};
    pos = Vector(0, 0, 0);
    priority = 0;
    discard = {};
    aStar = false;
    distance = 1500;

	constructor(pathsIn, posIn, priorityIn, discardFuncIn, distanceIn) {
        paths = pathsIn;
        pos = posIn;
        priority = priorityIn;
        discard = {};
        discard["func"] <- discardFuncIn;
        aStar = false;
        distance = distanceIn;
    }

    function getPos() {
        if(BotAI.validVector(pos))
            return pos;
        if(BotAI.IsEntityValid(pos) && "GetOrigin" in pos)
            return pos.GetOrigin();
        return Vector(0, 0, 0);
    }

    function discardFunc() {
        if("func" in discard)
            return discard["func"]();
        return true;
    }

    function _typeof () {
        return "PathData";
    }
}

class ::AreaData {
    area = null;
    lastArea = null;
    exactCost = 999999;
    estimatedCost = 999999;

	constructor(areaIn, lastAreaIn, exactCostIn, estimatedCostIn) {
        area = areaIn;
        lastArea = lastAreaIn;
        exactCost = exactCostIn;
        estimatedCost = estimatedCostIn;
    }

    function _typeof () {
        return "AreaData";
    }
}
